//Matthew Toledo 2033916

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d (double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;

    }
    
    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }


    public double getMagnitude() {
        double magnitude = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        return magnitude;
    }

    public double dotProduct(Vector3d vect2) {
        double dot = (this.x * vect2.getX()) + ( this.y * vect2.getY()) + (this.z * vect2.getZ());
        return dot;
    }

    public Vector3d add(Vector3d vect2) {
        double addX = (this.x + vect2.getX());
        double addY = (this.y + vect2.getY());
        double addZ = (this.z + vect2.getZ());
        
        Vector3d vectSum = new Vector3d(addX, addY, addZ);
        return vectSum;
    }
}