import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;
//Matthew Toledo 2033916
public class Vector3dTests {
    
        
    @Test
    public void getVectTest() {
        Vector3d vect = new Vector3d(3, 4, 5);
        assertEquals(3, vect.getX());
        assertEquals(4, vect.getY());
        assertEquals(5, vect.getZ());
    }

    @Test
    public void getMagnitudeTest() {
        Vector3d vect1 = new Vector3d(3, 4, 5);
        double mag = vect1.getMagnitude();
        assertEquals(Math.sqrt(50), mag);
        

    }

    @Test
    public void dotProductTest() {
        Vector3d vect1 = new Vector3d(3, 4, 5);
        Vector3d vect2 = new Vector3d(4, 5, 6);

        double vectDot = vect1.dotProduct(vect2);
        double expected = 12 + 20 + 30;
        assertEquals(expected, vectDot);
    }

    @Test
    public void addTest() {
        Vector3d vect1 = new Vector3d(3, 4, 5);
        Vector3d vect2 = new Vector3d(4, 5, 6);

        Vector3d vectSum = vect1.add(vect2);
        Vector3d expected = new Vector3d (7, 9, 11);
        assertEquals(expected.getX(), vectSum.getX());
        assertEquals(expected.getY(), vectSum.getY());
        assertEquals(expected.getZ(), vectSum.getZ());
    }
}
